<!DOCTYPE html>
<html lang="fr">

<body>
    <main>
        <h1 class="log1-h1 fw-bold mt-5 mb-5 text-uppercase rounded-3 text-center">LOGIN</h1>
        <form id="login" method="post" action="verif.php">
            <div class="form-outline form-white mb-4 rounded-5">
                <input type="text" name="text" placeholder="Pseudo" />
            </div>
            <div class="form-outline form-white mb-2 rounded-5">
                <input type="password" name="password" id="log_password" placeholder="Mot de passe" />
            </div>
            <button type="submit" name="connexion">CONNEXION</button>
        </form>

    </main>
</body>
</html>